; rdx = remainder

section .bss
	digitSpace resb 100
	digitSpacePos resb 8

section .text
	global _start

_start:
	mov rax, 123
	call _printRAX

	mov rax, 60
	mov rdi, 60
	syscall

_printRAX:
	mov rcx,digitSpace
	mov rbx, 10
	mov [rcx], rbx
	inc rcx
	mov [digitSpacePos], rcx
_printRAXLoop:
	mov rdx,0
	mov rbx, 10
	div rbx; rax/rbx
	push rax
	add rdx,48 ;ascii nonsense to get a character

	mov rcx, [digitSpacePos]
	mov [rcx], dl;lower 8 bytes of rdx
	inc rcx
	mov [digitSpacePos] ,rcx

	pop rax
	cmp rax,0
	jne _printRAXLoop

; we take 123/10=12 R 3
; store 3
;12/10 = 1 remainder 2
;store 2
;1/10 = 0 R 1
; store 1
_printRAXLoop2:
	mov rcx, [digitSpacePos]
	mov rax, 1
	mov rdi, 1
	mov rsi, rcx
	mov rdx, 1
	syscall

	mov rcx, [digitSpacePos]
	dec rcx
	mov [digitSpacePos] ,rcx

	cmp rcx, digitSpace
	jge _printRAXLoop2

	ret
