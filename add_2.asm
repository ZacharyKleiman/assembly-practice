%include "useful-stuff.asm"
section .bss
	digitspace resb 100

section .text
	global _start

_start:
	call _getNumber
	;call _addNumber
	;see if number is stored
	
	mov rax,SYS_WRITE
	mov rdi,STDOUT
	mov rsi,digitspace
	mov rdx,100
	syscall
	mov rax,SYS_WRITE
	mov rdi,STDOUT
	mov rsi, 10; newline
	;mov rdx, 10
	syscall

	mov rax, SYS_EXIT
	mov rdi, 0
	syscall

_getNumber:
	mov rax ,SYS_READ
	mov rdi, STDIN
	mov rsi, digitspace
	mov rdx, 100
	syscall
 	ret


;_atoi:
;	mov rax, 0              ; Set initial total to 0
;	mov rdi, [digitspace]
;_convert:
;  	movzx rsi, byte [rdi]   ; Get the current character;
;	test rsi, rsi           ; Check for \0
;	je done
;	cmp rsi, 48             ; Anything less than 0 is invalid
;	jl error
;	cmp rsi, 57             ; Anything greater than 9 is invalid
;	jg error
;	sub rsi, 48             ; Convert from ASCII to decimal
;	imul rax, 10            ; Multiply total by 10
;	add rax, rsi            ; Add current digit to total
;	inc rdi                 ; Get the address of the next character
;	jmp _convert
;error:
;    mov rax, -1             ; Return -1 on error
;
;done:
;    ret                     ; Return total or error code

;_convert2:
;	mov rax, 0              ; Set initial total to 0
;	mov rdi, [madeint]	
;	movzx rsi, byte [rdi]   ; Get the current character
;	test rsi, rsi           ; Check for \0
;	je done2
;	cmp rsi, 48             ; Anything less than 0 is invalid
;	jl error2
;	cmp rsi, 57             ; Anything greater than 9 is invalid
;	jg error2
;	add rsi, 48             ; Convert from decimal to ASCII
;	idiv rax, 10            ; divide total by 10
;	sub rax, rsi            ; Add current digit to total
;	inc rdi                 ; Get the address of the next character
;	jmp _convert2
;error2:
;   mov rax, -1             ; Return -1 on error

;done2:
;   ret                     ; Return total or error code

;_addNumber:
;	call _atoi
;	add rax,2
;	mov [madeint], rax ; from atoi
;	;convert to string
;	call _convert2
;	mov [restring],rax
	
	
;	mov rax,1
;	mov rdi, 1
;	mov rsi, [restring]
;	mov rdx, 100
;	syscall
;	mov rax, 1
;	mov rdi, 1
;	mov rsi, 10
;	mov rdx, 10
;	syscall
;	ret
