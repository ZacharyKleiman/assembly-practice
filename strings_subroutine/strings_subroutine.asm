section .data
	text db "Hello, World!", 10,0
	text2 db "ani adonai hamaaleh etchem meeretz mitzrayim lihyot lachem leelohim" , 10,0 
section .text
	global _start

_start:
	mov rax,text
	call _print
	mov rax, text2
	call _print
	mov rax, 60
	mov rdi, 0
	syscall

; input: rax as pointer to string
; output: print string at rax
_print:
	push rax; push rax on the stack
	mov rbx, 0 ;count string length
_printLoop:
	inc rax
	inc rbx
	mov cl, [rax] ;8 bit rcx
	cmp cl,0
	jne _printLoop ;if 0 it means its end
	mov rax, 1
	mov rdi, 1
	pop rsi
	mov rdx, rbx
	syscall
	ret
