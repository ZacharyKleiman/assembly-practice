; print number 100 after incrementing from 0

section .bss
	num resb 8
	numpos resb 8

section .text
	global _start
_start:
	mov rax, 0
	call _incrementRAX
	call _printRAX
	
	mov rax, 60
	mov rdi, 0
	syscall

_printRAX:
	mov rcx,num
        mov rbx, 10
        mov [rcx], rbx
        inc rcx
        mov [numpos], rcx
_printRAXLoop:
        mov rdx,0
        mov rbx, 10
        div rbx; rax/rbx
        push rax
        add rdx,48 ;ascii nonsense to get a character

        mov rcx, [numpos]
        mov [rcx], dl;lower 8 bytes of rdx
        inc rcx
        mov [numpos] ,rcx

        pop rax
        cmp rax,0
        jne _printRAXLoop

_printRAXLoop2:
	mov rcx, [numpos]
        mov rax, 1
        mov rdi, 1
        mov rsi, rcx
        mov rdx, 1
        syscall

        mov rcx, [numpos]
        dec rcx
        mov [numpos] ,rcx

        cmp rcx, num
        jge _printRAXLoop2
	ret

_incrementRAX
	inc rax
	cmp rax, 100
	jl _incrementRAX
	ret
