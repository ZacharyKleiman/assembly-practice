section .bss
	num resb 8
	numpos resb 8
section .text
	global _start
_start:
	mov rax,0
	mov r9,0
	push qword r9
	pop qword [num]
	call _inc_rax
	mov rax,60
	mov rdi, 60
	syscall

_callprintRAX:
	push rax
	call _printRAX
	pop rax
	
;	jmp _inc_rax
_inc_rax:
	inc rax
	cmp rax,100000
	jl _callprintRAX
;	jl _inc_rax
	ret
	
_printRAX:
;assumes value is in RAX
;assumes position of number is in buffer numpos
;assumes number is in num
;amends those names as necessary
	;your actual problem is that your printRAX destroys rax so your inc_rax loop never terminates
        mov rcx,num
        mov rbx, 10
        mov [rcx], rbx
        inc rcx
        mov [numpos], rcx
_printRAXLoop:
        mov rdx,0 ; to not break divide
        mov rbx, 10
        div rbx; rax/rbx
        push rax
        add rdx,48 ;ascii nonsense to get a character

        mov rcx, [numpos]
        mov [rcx], dl;lower 8 bytes of rdx
        inc rcx
        mov [numpos] ,rcx

        pop rax
        cmp rax,0
        jne _printRAXLoop
_printRAXLoop2:
        mov rcx, [numpos]
        mov rax, 1
        mov rdi, 1
        mov rsi, rcx
        mov rdx, 1
        syscall

        mov rcx, [numpos]
        dec rcx
        mov [numpos] ,rcx

        cmp rcx, num
        jge _printRAXLoop2
        ret
